The `rails-background-jobs` GDK service (essentially `sidekiq` )  can sometimes not shutdown cleanly and remain running, even after a `gdk stop`.
To rule that out, I suggest running:

```
gdk stop
sleep 5
ps -ef | egrep '[r]unsv'
```

There should be nothing returned ideally. If there's a `runsv` process, it'll usually be in a 'zombie' start (it's parent PID will be process ID 1) and if that's the case, you'll need to `kill <PID>`  and then re-run `ps -ef | egrep '[r]unsv'` to confirm it's gone.
Then `gdk start` and check