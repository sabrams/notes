# GraphQL

## Backend

### Mutations

For mutations start in `mutation_type.rb`

You must mount the mutation, only one mount per mutation (so delete and bulk delete are different).

`authorize :my_permission`

will check permission on the requested resource, so you need to have a policy for the resource, as in a containerrepository request will not check against the project policy, it will check against the containerrepoistory policy

You need to describe all incoming arguments and return fields(Types):

```
argument :id, Types::ID, required: true, description: 'foo'

field :container_repository, RepositoryTypes, description: 'foo'
```

The incoming params and the returned type needs to be described too, in this case container_repository is returned

`def resolve(arg1:, arg2:, **args)`

In resolve you can access the defined arguments by keyword, or get them all with **

the resolve needs to return:

```
{
  resource_object: obj,
  errors: obj
}
```

where resource_object is the object like `container_repository`

### Types

### Resolvers

### Queries

You need to return whatever type you are querying.

-----

## Frontend
