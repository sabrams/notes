## Workhorse notes

To make changes locally in GDK:

```
make gitlab-workhorse-setup && gdk restart gitlab-workhorse
```
