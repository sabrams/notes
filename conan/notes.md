# Conan Package Registry - Notes

The [Conan CLI](https://docs.conan.io/en/latest/reference/commands.html) commands generally used are:

```
conan install // installs package and package dependencies
conan search // searches for conan packages locally or on remotes, default is conan-center
conan upload // publishes/uploads a package to a remote
conan info // displays info about a given package
```
Each command makes requests for multiple API endpoints, and depending on the state of the package, local repository, and remote repository, the number and order of these API calls will vary each time a command is performed.

## Package Files & Storage Structure

There are a handful of files stored using the following paths, these paths are also used in some of the API calls. Here is an example of the conanfile.py recipe file:

`./data/project_name/version/user/channel/0/export/conanfile.py`

The data folder is used locally in `~./.conan/`, which is the path for the local conan repository to store packages.

`project_name/version/user/channel` is the path/url equivelant of the package recipe: `project_name/version@user/channel`

The `0` is the revision of the recipe. Revisions only exist in v2 of the conan API, currently, GitLab and most users use v1, in which there is only one revision, which is `0` by default. This is hardcoded in GitLab at the moment.

There are two recipe files:

```
/{recipe}/{recipe_revision}/export/conanfile.py
/{recipe}/{recipe_revision}/export/conanmanifext.txt
```

Package files extend the recipe path a bit more. For example, the actual package .tgz file path looks like:

`./data/project_name/version/user/channel/0/package/2308hpjir2h89023wfp9/0/conan_package.tgz`

The `2308hpjir2h89023wfp9` portion is the conan "package_id". The 2nd `0` is the package revision, also defaulting to `0` in v1 of the conan API.

There are three package files:

```
/{recipe}/{recipe_revision}/package/{package_id}/0/conan_package.tgz
/{recipe}/{recipe_revision}/package/{package_id}/0/conaninfo.txt
/{recipe}/{recipe_revision}/package/{package_id}/0/conanmanifext.txt
```

There are also two possible revision files that come into play with the v2 conan API, but we have not implemented them in GitLab, these paths are noted only for reference:

There are three package files:

```
/{recipe}/{recipe_revision}/package/{package_id}/revisions.txt
/{recipe}/{recipe_revision}/package/{package_id}/revisions.txt.lock
```

## Package Recipes

A package recipe is in the format

`project_name/version@user/channel`

In GitLab's package registry, the project_name and version can be anything. They are set in the conanfile.py that is used to define the package. channel can also be anything, it is often seen as a "tag", using values such as "stable", or "beta". The value for user usually can also be anything, but for GitLab, it is the full path of the project that is being published to, seperated by periods. so if I was publishing to `https://gitlab.com/package-stage/team`, the user value would be `package-stage.team`. Thus periods cannot be used in the path or the project cannot be identified. There is a note of this in the docs, but nothing preventing users from attempting to publish to a given path this way.

> Note to self: perhaps the project_id should be interchangeable with the path here to allow users to use projects with periods in them

## "conans" endpoints

- `base (snapshot)` - returns hash of all files and their md5 hash
- `/digest` - returns hash of all files export/package & download url: `{ "manifext.txt": "http://.../manifest.txt" }
- `/upload_urls` - returns has of: `{ filepath: url } - The upload url can be anything, the client will just use it.
- `/download_urls` - returns has of: `{ filepath: url } - The client will just make a GET for the download_url
> download and digest may have the same response?

## Installing packages

The conan.txt file is synonymous to the package.json file in javascript. It is used to list dependencies and options. the contents look like this:

```
[requires]
  Hello/0.1@user/channel

[generators]
  cmake

[options] // used to list conan config options that would normally be passed with `-o` in CLI

[imports]
```

The requires section lists the packages to be installed on a project.

With this list in place, you can run
```
$ mkdir build && cd build
$ conan install ..
```
And the dependencies will be installed. Note the `..` is part of the command.

## Making a new package

To create a package run:
```
$ mkdir mypkg && cd mypkg
$ conan new Hello/0.1 -t
```

To build a package
```
$ conan create . user/channel
```

It will generate some files, the important of which is `conanfile.py`. It contains the name and version of the package, which will be used in the recipe.

## Setting the remote

While developing locally with Conan, you can set the remote with:

`conan remote add localhost http://localhost:3001/api/v4/packages/conan`

You will need to generate a personal access token for registry authentication.

Now you can run conan commands using this remote:

`CONAN_LOGIN_USERNAME=<any-name> CONAN_PASSWORD=<PAT> conan upload Hello/0.2@user/channel --remote=localhost`

## Local development better logging

For logging, in `~/.conan/conan.conf`

Add a trace file, set level to 10, set verbose traceback. This will show the api requests in the output and the trace file, and it will show error traces in the output.

```

[log]
run_to_output = True        # environment CONAN_LOG_RUN_TO_OUTPUT
run_to_file = False         # environment CONAN_LOG_RUN_TO_FILE
level = 10                  # environment CONAN_LOGGING_LEVEL
trace_file = /Users/steveabrams/.conan/trace.log             # environment CONAN_TRACE_FILE
print_run_commands = False  # environment CONAN_PRINT_RUN_COMMANDS

[general]
default_profile = default
compression_level = 9                 # environment CONAN_COMPRESSION_LEVEL
sysrequires_sudo = True               # environment CONAN_SYSREQUIRES_SUDO
request_timeout = 60                  # environment CONAN_REQUEST_TIMEOUT (seconds)
default_package_id_mode = semver_direct_mode # environment CONAN_DEFAULT_PACKAGE_ID_MODE
# retry = 2                             # environment CONAN_RETRY
# retry_wait = 5                        # environment CONAN_RETRY_WAIT (seconds)
# sysrequires_mode = enabled          # environment CONAN_SYSREQUIRES_MODE (allowed modes enabled/verify/disabled)
# vs_installation_preference = Enterprise, Professional, Community, BuildTools # environment CONAN_VS_INSTALLATION_PREFERENCE
verbose_traceback = True           # environment CONAN_VERBOSE_TRACEBACK
```

## Uploading a package to GitLab (or localhost running GitLab)

`CONAN_LOGIN_NAME=sabrams CONAN_PASSWORD=<personal_access_token> conan upload Hello/0.1@user/channel --all --remote=localhost`

## Searching for packages

`CONAN_LOGIN_NAME=sabrams CONAN_PASSWORD=<personal_access_token> conan search Hello --all --remote=localhost`

`CONAN_LOGIN_NAME=sabrams CONAN_PASSWORD=<personal_access_token> conan search Hel* --all --remote=localhost`

## Remove a package from local conan (in order to test install and other functionality)

`conan remove Hello/0.1@user/channel`

## References

[Kras's Snippet](https://gitlab.com/snippets/1873948)
