# Day 3 - I/O, memory

* DDL statements (Data definition language) - this just means anything that creates or modifies the database objects such as tables, indexes, users. Changes to the DB structure

## Functions

Dollar quoting:

`$abc$hello$abc` is the same as `'hello'` allows you to then use single quotes in a string

`CREATE FUNCTION hello() RETURNS text LANGUAGE sql AS $$SELECT 'hello world'$$;`

`\h CREATE FUNCTION` to see various types of functions (Language, Table, Window, etc)

```sql
-- these have different costs, you can see details with VERBOSE
EXPLAIN (VERBOSE) SELECT pi() FROM test; -- pi is STABLE/Immutable FUNCTION
EXPLAIN (VERBOSE) SELECT random() FROM test; -- calls a function for each row.
```

### Function inlining

Function is integrated into the SQL statement

```sql
CREATE FUNCTION addone(integer) returns integer Lanaguage sql as 'select $1 + 1';

EXPLAIN (VERBOSE) SELECT addone(id::integer) FROM TEST; -- id::integer is casting id to an integer
```

### Functions with Triggers

One of the main use cases for functions is triggers.

- Trigger function `RETURNS trigger`

#### Triggers

  - Triggers always work on one table. Using a function, you could set triggers on multiple tables (call the function on multiple tables).
  - `\h CREATE TRIGGER`
  - Triggers are caused by an event (insert, update, delete), they can happen BEFORE, AFTER, or INSTEAD OF the event.
  - BEFORE triggers can modify data before it hits the table. Also useful if you want to cancel the event (you can add validation at SQL level)
  - AFTER triggers are good with DEFERRABLE (by default, all triggers are NOT DEFERRABLE). If you set it as DEFERRABLE, you can delay the trigger at the end of the transaction.
  - There are [FOR EACH] ROW or STATEMENT triggers: should it run for each row, or once per statement.

#### Example - History auditing

- **Note**: You can use `\e` to open editor (vim), type a large statement, it won't be run until you exit the editor

```sql
-- \e
CREATE TABLE realdata (
        id bigint GENERATED BY DEFAULT AS IDENTITY NOT NULL,
        value text,
        valid tstzrange DEFAULT tstzrange(current_timestamp, NULL) NOT NULL,
        EXCLUDE USING gist (valid WITH &&, id WITH =)
);

CREATE VIEW data AS SELECT id, value FROM realdata WHERE valid @> current_timestamp;

-- \e
-- This function can be used to add data to realdata depending on an event on another table.
-- On INSERT it creates a new row on newdata with the value
-- On DELETE it updates the row on newdata changing valid range
-- On UPDATE it does both
CREATE OR REPLACE FUNCTION data_trig() RETURNS trigger
   LANGUAGE plpgsql AS
$$
BEGIN
   CASE TG_OP
      WHEN 'INSERT' THEN
         IF NEW.id IS NULL THEN
            INSERT INTO realdata(value) VALUES (NEW.value);
         ELSE
            INSERT INTO realdata(id, value) VALUES (NEW.id, NEW.value);
         END IF;
         RETURN NEW;
      WHEN 'DELETE' THEN
         UPDATE realdata
            SET valid = tstzrange(lower(valid), current_timestamp)
            WHERE id = OLD.id AND valid @> TIMESTAMPTZ 'infinity';
         RETURN OLD;
      WHEN 'UPDATE' THEN
         UPDATE realdata
            SET valid = tstzrange(lower(valid), current_timestamp)
            WHERE id = OLD.id AND valid @> TIMESTAMPTZ 'infinity';
         INSERT INTO realdata(id, value) VALUES (NEW.id, NEW.value);
         RETURN NEW;
   END CASE;
END;
$$;

CREATE TRIGGER data_trig INSTEAD OF INSERT OR UPDATE OR DELETE ON data FOR EACH ROW EXECUTE PROCEDURE data_trig(); -- data is the table we want to add auditing to, we are adding a trigger to it, so when it is updated, realdata is updated via the trigger. Note that data only shows "valid" realdata.

-- do some inserts onto data
INSERT INTO data (value) VALUES ('first');
TABLE data;
UPDATE data SET value = 'changed' WHERE id = 1;
UPDATE data SET id = 2 WHERE id = 1;
TABLE data;
DELETE FROM data WHERE id = 1;
DELETE FROM data WHERE id = 2;

-- look at the difference now
TABLE data;
TABLE realdata;

-- now we can time travel!
SELECT id, value FROM realdata WHERE valid @> TIMESTAMPTZ '2020-04-29 17:18:37';

-- is it efficient?
EXPLAIN SELECT id, value FROM realdata WHERE valid @> TIMESTAMPTZ '2020-04-29 17:18:37';
-- yes, it uses the gist index!
```

```sql
CREATE EVENT TRIGGER -- triggered by ddl statments
```

Potential use: This could be triggered whenever anyone says "drop", do not allow users to drop any table.

### Materialized views

A view is just an SQL statement, a materialized view is more like a table because it is an executed query and actually stores data. It is kind of like a cached view/table, it can become stale over time.

## Procedures

`\h CREATE PROCEDURE`


# I/O

Shared buffers is like the cache (memory)

Then we have the disk. Everything on the disk is arranged in the 8kb blocks we described. Everything that is read or written is passed through shared buffers. The backend never actually talks directly to the disk. The block is loaded into the shared buffers and that is where things are managed. The block stays in cache and accumulates a "use count", it eventually degrades, every process that looks for a new block needs to find free space in shared buffers, it's common to not have free space. So you walk through the cache, and for each block you walk past that you don't use, you decrement the "use count", when you reach a 0 block, it is removed and that is where your new block goes.

When writing data, things get more complicated. The WAL (write ahead log) comes into play. Shared memory has a few extra pieces, WAL buffers and lock table (along with shared buffers).

Lifecycle of a data change:

1. read block into shared buffers from disk

1. the backend writes a log entry for the change in WAL buffers. A change must be logged here before adding it to the system. This is in case something happens, we don't want data to hit disk that is not logged (if we died in the middle of writing a block, we would have data inconsistencies if we didn't have this log, used to repair after a crash)

1. We may check the lock table now

1. The WAL buffer wrties to the WAL (transaction log). Then the transaction is persisted.

1. The dirty block is kept in the shared buffer for a while, that's ok.

1. the "check pointer" writes the new data from the shared buffer to the disk (background writer)

You can see most of these processes running with:

`ps -fu steveabrams | grep postgres:`

Some databases use "Direct I/O", postgres does not do this, it uses "Buffered I/O"

example:

```sql
CREATE TABLE waltest (id integer);
```

Then create a script `insert.sql` that inserts 30,000 rows.

```sql
INSERT INTO waltest VALUES (1);
```

```sql
time psql -q -f insert.sql
-- real 0m14.269s
```

Why did this take so long? Postgres works in autocommit mode: a single transaction for each insert (30000), each transaction has to make sure that the WAL file is on disk, so we get 30000 fsync requests to ensure that the write to WAL is persisted on disk

If we change this to a single transaction:

```sh
vim insert.sql

wrap file in

BEGIN;

COMMIT;
```

rerun

```sql
time psql -q -f insert.sql
-- real 0m1.832s
```

 This results in a single transaction, a single fsync.

# Configuration parameters

`SET enable_seqscan = off;` this only works for the single session, not a global setting.

```sh
cd /usr/local/var/postgres/

ls

ls base
-- we see database by their object id (oid)

inside a base folder, we see many files that are tables and other objects. _vm (visibility map for index only scans).

`postgresql.conf`


```

`SHOW enable_seqscan;` shows current value

## `postgresql.conf` file

### Important params

#### shared_buffers

Can only be changed in the config file, can't be changed per session.

`shared_buffers = 128MB` This is small, should be larger on a new install, how big should it be? GitLab's is 112+GB. There is no good answer. A rule of thumb to start is start with 1/4 memory available to DB but not more than 8 GB. 8GB might seem low, but we used Buffered I/O, so it is still fast. Perfomance test is the only way to really know and then tune the value from there.

Check out `\d pg_buffercache` can be queried to check things out

`select usagecount, count(*) from pg_buffercache group by 1 order by 1;` Shows usage count of each block. If there's a high usage count for many items, it's haard to find open cache space, so we might benefit from larger shared_buffers. If we have low usage count, perhaps lower shared_buffers, or no change.

It's hard to play with this value because you have to restart the db to change it.

#### work_mem

This is tunable per session with `SET`

`work_mem = 4MB` (Gitlab is 100MB)

It's background memory. Every process can use work memory.

```sql
postgres# explain select * from pg_stats;
                                             QUERY PLAN
════════════════════════════════════════════════════════════════════════════════════════
 Nested Loop Left Join  (cost=115.41..145.02 rows=6 width=401)
   ->  Hash Join  (cost=115.27..143.41 rows=6 width=475)
         Hash Cond: ((s.starelid = c.oid) AND (s.staattnum = a.attnum))
         ->  Seq Scan on pg_statistic s  (cost=0.00..22.48 rows=448 width=349)
         ->  Hash  (cost=102.31..102.31 rows=864 width=142)
               ->  Hash Join  (cost=20.55..102.31 rows=864 width=142)
                     Hash Cond: (a.attrelid = c.oid)
                     Join Filter: has_column_privilege(c.oid, a.attnum, 'select'::text)
                     ->  Seq Scan on pg_attribute a  (cost=0.00..74.92 rows=2592 width=70)
                           Filter: (NOT attisdropped)
                     ->  Hash  (cost=16.27..16.27 rows=342 width=72)
                           ->  Seq Scan on pg_class c  (cost=0.00..16.27 rows=342 width=72)
                                 Filter: ((NOT relrowsecurity) OR (NOT row_security_active(oid)))
   ->  Index Scan using pg_namespace_oid_index on pg_namespace n  (cost=0.13..0.19 rows=1 width=68)
         Index Cond: (oid = c.relnamespace)
(15 rows)
```

work_mem is the upper limit that each node shown in the explain plan can use.

For example, looking at Seq Scan on pg_class, it uses almost no memory.

3 nodes are memory hungry: Hash, Bitmap index scan (scans index and creates bitmap in memory), Sort (the more the rows to sort, the more memory needed).

EXAMPLE

```sql
postgres# CREATE TABLE mem (id serial, name text);
CREATE TABLE
postgres# insert into mem (name) select 'carl' from generate_series(1, 100000);
INSERT 0 100000
postgres# insert into mem (name) select 'paul' from generate_series(1, 100000);
INSERT 0 100000
postgres# analyze mem;
ANALYZE
postgres# vacuum mem;
VACUUM
postgres# -- now in mint condition
postgres# explain select name, count(*) from mem group by name;
                                         QUERY PLAN
════════════════════════════════════════════════════════════════════════════════════════════
 Finalize GroupAggregate  (cost=3846.75..3847.01 rows=2 width=13)
   Group Key: name
   ->  Gather Merge  (cost=3846.75..3846.98 rows=2 width=13)
         Workers Planned: 1
         ->  Sort  (cost=2846.74..2846.74 rows=2 width=13)
               Sort Key: name
               ->  Partial HashAggregate  (cost=2846.71..2846.73 rows=2 width=13)
                     Group Key: name
                     ->  Parallel Seq Scan on mem  (cost=0.00..2258.47 rows=117647 width=5)
(9 rows)

postgres# SET max_parallel_workers_per_gather = 0;
SET
postgres# explain select name, count(*) from mem group by name;
                           QUERY PLAN
═════════════════════════════════════════════════════════════════
 HashAggregate  (cost=4082.00..4082.02 rows=2 width=13)
   Group Key: name
   ->  Seq Scan on mem  (cost=0.00..3082.00 rows=200000 width=5)
(3 rows)

postgres# explain select id, count(*) from mem group by id;
                              QUERY PLAN
═══════════════════════════════════════════════════════════════════════
 GroupAggregate  (cost=23428.64..26928.64 rows=200000 width=12)
   Group Key: id
   ->  Sort  (cost=23428.64..23928.64 rows=200000 width=4)
         Sort Key: id
         ->  Seq Scan on mem  (cost=0.00..3082.00 rows=200000 width=4)
(5 rows)
-- all 200000 are loaded into work_mem, so we can't use HashAggregate, it woudln't fit, so the database decided to use Sort instead

-- lets change work_mem now
postgres# SET work_mem = '1GB';
SET
postgres# explain select id, count(*) from mem group by id;
                           QUERY PLAN
═════════════════════════════════════════════════════════════════
 HashAggregate  (cost=4082.00..6082.00 rows=200000 width=12)
   Group Key: id
   ->  Seq Scan on mem  (cost=0.00..3082.00 rows=200000 width=4)
(3 rows)

-- Now we can use the more efficient hashaggregate

-- using larger work_mem can make some things much more efficient, especially sorting where we can load it all into memory. At some size we eventually spill to disk and things slow own (1 million rows). but it's nice to sort perhaps 100000 rows in mem.

-- Same with bitmap index scan. if the bitmap fits in memory, it will use the work_mem, otherwize the bitmap will degrade and be slower.

RESET work_mem;
```

RAM >= shared_buffers + work_mem * max_connections

#### max_connections

Very important and underappreciated in setting correctly, default is 100

100 is somewhat high, definitely don't want to go higher than that (GitLab is 300, not atypical)

Too many connections can flood DB with traffic after an unfortunate long lock

Usually most connections are idle. The solution is to use connection pooler

It is a layer between the many app instances and the db. Instead of a pool per app, use a layer between that accepts many connections from client/app, but it has a total per cluster. Maybe set a max of 800

Then there is a limit to the number of connections to the DB, maybe 30. It is kind of like a load balancer to the DB. Most of the time, we don't need to worry, but in the flood scenario, it prevents the DB from failing, 30 max, the rest just wait in pgbouncer, it creates a natural controlled bottleneck (buffer) of higher activity. Response time will be longer for those that have to wait, but we succeed in never overloading the DB.

**GITLAB'S CONNECTION POOLER IS pg Bouncer!!!**

#### synchronous_commit

Default to `on`

If you turn it off, it means not every commit causes a flash of the transaction log. Could potentially lose a half second of transactions in a crash or something. Not to be turned off for things like money transactions. This bumps performance a lot by removing all the fsyncs

commit_delay and commit_siblings can also be used to help