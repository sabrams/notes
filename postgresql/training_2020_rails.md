# Rails Perf

## Active Record

Active record was actually a design pattern laid out by Martin Fowler in [`Patterns of Enterprise Application Architecture`](https://www.amazon.com/Patterns-Enterprise-Application-Architecture-Martin/dp/0321127420).

Each active record insert is wrapped in a transaction (BEGIN...COMMIT), you can see this in the rails console.

Instantiating objects is a slow process and a common error in rails. There are some features not supported by default in active record.

Partitioning is not supported by default in active record. However there are many gems that can be used to work with this.

`rails db` opens connection to local db client for rails app

### database.yml

There are some specific PG params we can set in the `database.yml`:

```ruby
variables: # you can set the PG spefic things using SET sql statements here in a hash

encoding: utf-8
```

### Datatypes

#### Binary

```ruby
t.binary
```

Limited to 1GB storage, and allows non-printed characters (string does not).

Good for storing files (not a common practice to store files in DB though)

#### Array

```ruby
    add_column :users, :emails, :string, array: true, default: []
```

```sql
\d users
                                            Table "public.users"
       Column       │            Type             │ Collation │ Nullable │              Default
════════════════════╪═════════════════════════════╪═══════════╪══════════╪═══════════════════════════════════
 id                 │ bigint                      │           │ not null │ nextval('users_id_seq'::regclass)
 username           │ character varying           │           │          │
 encrypted_password │ character varying           │           │          │
 created_at         │ timestamp without time zone │           │ not null │
 updated_at         │ timestamp without time zone │           │ not null │
 emails             │ character varying[]         │           │          │ '{}'::character varying[]
Indexes:
    "users_pkey" PRIMARY KEY, btree (id)
```

Allows us to create multidimensional arrays of any type known to postgresql.

Note, postgres uses {} for arrays, ruby uses []. We need to keep this in mind in writing queries.

```ruby
User.where("emails @> '{alphonse@paucek.co}'").explain

User Load (3.2ms)  SELECT "users".* FROM "users" WHERE (emails @> '{jacinto@okunevaleffler.name}')
=> EXPLAIN for: SELECT "users".* FROM "users" WHERE (emails @> '{jacinto@okunevaleffler.name}')
                                 QUERY PLAN
----------------------------------------------------------------------------
 Seq Scan on users  (cost=0.00..4.26 rows=1 width=179)
   Filter: (emails @> '{jacinto@okunevaleffler.name}'::character varying[])
(2 rows)
```

##### Add a gin index

```ruby
add_index :users, :emails, using: 'gin'
```

#### Json/Jsonb

```ruby
t.json :col
t.jsonb :col
```

Both are almost the same, json stores the data as an exact copy in pure text.

Jsonb stores data in a composed binary form. This has benefits such as more efficient processing, indexing, faster. Drawbacks are slightly slower input due to conversion overhead, but it is negligible. Another drawback is it may take more disk space due to large table footprint. Searching queries (especially aggregate) are slower due to lack of statistics.

General recommendation, if you are using mostly read, use jsonb. If you are doing a lot of write/read, maybe use plain json.

`->>` contains operator for json

#### Range

Range datatype is mapped to ruby range datatype, makes easy to use.

```ruby
t.daterange :duration


Event.create(duration: Date.new(2014, 2, 11)..Date.new(2014, 2, 12))
   (0.3ms)  BEGIN
  Event Create (2.2ms)  INSERT INTO "events" ("duration", "created_at", "updated_at") VALUES ($1, $2, $3) RETURNING "id"  [["duration", "[2014-02-11,2014-02-12]"], ["created_at", "2020-05-04 16:46:54.492219"], ["updated_at", "2020-05-04 16:46:54.492219"]]
   (3.6ms)  COMMIT
=> #<Event id: 1, duration: Tue, 11 Feb 2014..Wed, 12 Feb 2014, created_at: "2020-05-04 16:46:54", updated_at: "2020-05-04 16:46:54">

Event.first.duration
=> Tue, 11 Feb 2014...Thu, 13 Feb 2014

Event.where("duration @> ?::date", Date.new(2014, 2, 12))
  Event Load (4.2ms)  SELECT  "events".* FROM "events" WHERE (duration @> '2014-02-12'::date) LIMIT $1  [["LIMIT", 11]]
=> #<ActiveRecord::Relation [#<Event id: 1, duration: Tue, 11 Feb 2014...Thu, 13 Feb 2014, created_at: "2020-05-04 16:46:54", updated_at: "2020-05-04 16:46:54">]>

event = Event.select("lower(duration) AS starts_at").select("upper(duration) AS ends_at").first
  Event Load (0.7ms)  SELECT  lower(duration) AS starts_at, upper(duration) AS ends_at FROM "events" ORDER BY "events"."id" ASC LIMIT $1  [["LIMIT", 1]]
=> #<Event id: nil>

event.starts_at
=> Tue, 11 Feb 2014

event.ends_at
=> Thu, 13 Feb 2014
```

#### UUID

Alternative primary key type for sql database. Some non-obvious advantages to standard integers.

Rails 6 makes it easier to work with UUID primary keys.

Pros:

* Non-discoverable like integer values are. `/orders/2345/checkout`, uuids prevent this visibility.

```ruby
def change
  enable_extension 'pgcrypto' #important

  create_table :people, id: :uuid do |t|
    t.string :name
    t.timestamps
  end
end
```

You can setup an initializer to use uuids for all primary keys

```ruby
# config/initializers/generators.rb

Rails.application.config.generators do |g|
  g.orm :active_record, primary_key_type: :uuid
end
```

### Transactions

```ruby
irb(main):018:0> Account.create(name: 'Carlos', amount: 1500)
   (0.1ms)  BEGIN
  Account Create (4.8ms)  INSERT INTO "accounts" ("name", "amount", "created_at", "updated_at") VALUES ($1, $2, $3, $4) RETURNING "id"  [["name", "Carlos"], ["amount", "1500.0"], ["created_at", "2020-05-04 17:09:01.935424"], ["updated_at", "2020-05-04 17:09:01.935424"]]
   (0.3ms)  COMMIT
=> #<Account id: 1, name: "Carlos", amount: 0.15e4, created_at: "2020-05-04 17:09:01", updated_at: "2020-05-04 17:09:01">
irb(main):019:0> Account.create(name: 'Robert', amount: 3000)
   (0.2ms)  BEGIN
  Account Create (0.3ms)  INSERT INTO "accounts" ("name", "amount", "created_at", "updated_at") VALUES ($1, $2, $3, $4) RETURNING "id"  [["name", "Robert"], ["amount", "3000.0"], ["created_at", "2020-05-04 17:09:15.512878"], ["updated_at", "2020-05-04 17:09:15.512878"]]
   (0.5ms)  COMMIT
=> #<Account id: 2, name: "Robert", amount: 0.3e4, created_at: "2020-05-04 17:09:15", updated_at: "2020-05-04 17:09:15">
irb(main):020:0> account = Account.first
  Account Load (0.2ms)  SELECT  "accounts".* FROM "accounts" ORDER BY "accounts"."id" ASC LIMIT $1  [["LIMIT", 1]]
=> #<Account id: 1, name: "Carlos", amount: 0.15e4, created_at: "2020-05-04 17:09:01", updated_at: "2020-05-04 17:09:01">
irb(main):021:0> d_account = Account.last
  Account Load (0.4ms)  SELECT  "accounts".* FROM "accounts" ORDER BY "accounts"."id" DESC LIMIT $1  [["LIMIT", 1]]
=> #<Account id: 2, name: "Robert", amount: 0.3e4, created_at: "2020-05-04 17:09:15", updated_at: "2020-05-04 17:09:15">
irb(main):022:0> Account.transaction do
irb(main):023:1* account.withdraw(500)
irb(main):024:1> d_account.deposit(500)
irb(main):025:1> end
   (0.3ms)  BEGIN
  Account Update (0.4ms)  UPDATE "accounts" SET "amount" = $1, "updated_at" = $2 WHERE "accounts"."id" = $3  [["amount", "1000.0"], ["updated_at", "2020-05-04 17:10:08.420290"], ["id", 1]]
  Account Update (0.2ms)  UPDATE "accounts" SET "amount" = $1, "updated_at" = $2 WHERE "accounts"."id" = $3  [["amount", "3500.0"], ["updated_at", "2020-05-04 17:10:08.427085"], ["id", 2]]
   (2.7ms)  COMMIT
=> true
```

If an error is raised in the transaction, it will trigger a postgresql rollback.

```ruby
Account.transaction do
...
end

d_account.transaction do
...
end

ActiveRecord::Base.transaction do
...
end
```

The execution for all of these is the same rails opens a pg transaction.

`save!` will raise an exception if something bad happens, `save` will just throw a true or false, not triggering a rollback on the save failure. So in transactions, it's important to use `save!` notation.

There are two callbacks we can use for transactions:

```ruby
after_commit :do_something

after_rollback :do_something_else
```

#### Rescuing transactions

```ruby
def transfer_funds_to(aamount, target)
  self.amount -= amoun t
  target.amount += amount
  Account.transaction do
    self.save! && target.save!
  end
rescue ActiveRecord::InvalidType => e
  puts "Oops"
end
```

```ruby
def copy_invoice(original)
  ActiveRecord::Base.transaction do
    duplicate = Invoice.create!(original.recipient)
    original.items.each do |item|
      duplicate.items.create!(article: item.article, amount: item.amount)
    end
  end
end
```

### Partitions

[`gem pg_party`](https://github.com/rkrage/pg_party)

Methods for creating range/list partitions. Methods for attaching/detaching partitions.

Good docs with lots of examples.

## Best practices

* Let the DB do what it's good at rather than doing it in ruby.
* Always filter in DB, not in ruby
* Always order in DB, not in ruby

### Scopes

Key is writing efficient and chainable scopes

Don't do things like:

* return something that is not an ActiveRecord::Relation
* `order` scopes should be separated

### Raw SQL

```ruby
query = <<~SQL
  SELECT
    SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),
    ...
  FROM
    PROJECTS
SQL

ActiveRecord::Base.connection.execute(query)
```

# Helpful gem

[`gem rails-pg-extras`](https://github.com/pawurb/rails-pg-extras)

Shows locks, seq scan count, many many more
