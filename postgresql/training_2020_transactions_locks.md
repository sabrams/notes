# Day 2 - Transactions and Locking

### A - Atomicity

A transaction is completed, or nothing has happened, there is no partial transactions. It succeeds completely or it doesn't succeed at all.

### C - Consistency

FK constraints, certain constraints are always respected

### I - Isolation

- Read Uncommitted - doesn't really exist in postgres
- Read Committed - guarantees no dirty reads, if you are in a transaction and someone else commits, you see the updated (committed) data.
- Repeatable Read - if I run the same statement in a transaction, it should return the same result (if you are in a transaction and someone else commits, you do not see the update, you are frozen in the state you started the transaction).

- Serializable

### D - Durability

## Traansactions

Every SQL statement always runs in a transaction. Postgres has autocommit mode, if you don't explicitly say anything, each command will run in it's own transaction.

If we want a transaction to be longer than one command:

```sql
-- to start a transaction
START TRANSACTION;
-- or
BEGIN;

--to end transaction

COMMIT; -- does the work

ROLLBACK; -- all work is undone

\q -- will cause a rollback
```

## Savepoints (Subtransactions)

`SAVEPOINT a;`

sets a place you can go back to mid transaction, we can rollback just the subtransaction (things that happened after savepoint).

`ROLLBACK TO SAVEPOINT a;`

Try not to use them too much, there is a cutoff of 64.

```sql
\set

ON_ERROR_ROLLBACK='off' -- transacti\ons will break
ON_ERROR_ROLLBACK='interactive' -- transactions will not break, but this is being handled by psql client, not by the postgresql server

\set ON_ERROR_ROLLBACK 'off'
```

### A note on numeric datatypes

```sql
SELECT DOUBLE PRECISION '3.14'; -- float8 (imprecise, but fast)
SELECT REAL '3.14'; -- float4 (imprecise, but fast)
SELECT NUMERIC '3.14'; -- numeric (precise, but slower, good for exactness in math)
```

## Pessamistic Locking (expect others to interfere)

- Read committed isolation level

- We proactively lock the row, we don't want others to interfere and make updates at the same time, we want to know that our update will happen before anything else that happened after we locked the row.

```sql
SELECT amount FROM account WHERE id = 2 FOR UPDATE; -- locks the row, others can't do anything, but if someone else updates, when this update is over, the other will complete after, so if we both update the same thing, the other will show, and this transaction update will seem as if it did not happen. (they occured in order though)

SELECT amount FROM account WHERE id = 2 FOR UPDATE OF account;

SELECT amount FROM account WHERE id = 2 FOR UPDATE NOWAIT;

SELECT amount FROM account WHERE id = 2 FOR UPDATE SKIP LOCKED;
```

## Optimistic Locking (likely no one will interfere, risk failing)

- Repeatable read isolation level

- We do not proactively lock the row right away, we also don't want to interfere if someone else is making updates, we want to trust that we want their update to happen, but perhaps it will break our update

```sql
BEGIN ISOLATION LEVEL REPEATABLE READ;
```

If you both update in this state, there will be an error: `could not serialize access due to concurrent update`

## How does this all work?

`SELECT ctid, xmin, xmax, * from account`

ctid is the actual address in the block, xmin and xmax are the transaction ids, which point to the different row versions, the row version updates (location on the block is different). Vaccuum cleans it up. We basically get a version control on each row using transactions.

about 1.5 hours into the talk.

- Explicit table locks `LOCK TABLE table` is bad style, don't do this.

## Concurrency

`ACCESS EXCLUSIVE` locks are used when modifying the table (add, rename, remove rows, etc..), a common topic at GitLab. One notable statement that requires this lock is create/drop index.

There is a variant of create index called `CREATE INDEX CONCURRENTLY`, postgres does not lock the table, but starts building the index in the background, then when it's just about ready, it locks the table and finishes the job.

Consider we have long running select statements like report queries. Then we have an add column, very fast, but it has to queue behind the `ACCESS SHARE` statement to complete because it needs the `ACCESS EXCLUSIVE` lock. More selects show up, they can run in parallel, but since we have this exclusive queued behind the current select, all of the incoming statements queue behind the exclusive statement. So work piles up and everything hangs. This flood of activity could overload the machine. There is no easy way around this, we need to be able to change things. How do we solve this? We keep transactions short. Short transactions are the key to happiness in databases.

It would be nice to look at these locks to diagnose them. pg_locks

Id
```sql
-- in a transaction, you can do
SELECT pg_backend_pid();
-- to see the pid of a transaction

-- then in another session
SELECT * from pg_locks where pid = 1234;
-- will show you locks
```

in the table, `mode` is the lock type, `granted` is `f` if it's waiting for a lock. `pid` is the transaction, `relation` is the object id `oid` of the table

you can see the oid with:

`select oid from pg_class where relname = 'account';`

to kill a pid `pg_terminate_background(pid);`

### Deadlocks

It is a serialization error, when concurrent processes do concurrent work. If a deadlock occurs, you don't have to throw an error, you could just try to repeat the transaction. They shouldn't be a big deal if they are handled correctly. Things get bad if you have lots and lots of these,

Make a rule, "if you want to make a money transfer, always modify the account with the lower value first". Similar to a bow and arrow, if one person grabs a bow and the other the arrow, you are locked, set up a rule to always pickup the bow first, so no one will pick up the arrow first causing a deadlock.

## Serializable Isolation Level

A transaction is seriazliabel if there is a serializable execution, you have the database to yourself, you execute one at a time without error, more isolated than repeatable read.

"Do not allow anyone to do ANYTHING until I'm done with this transaction"

```sql
BEGIN ISOLATION LEVEL SERIALIZABLE;
```

You can repeat the transaction and it may succeed happily, but then you are positive that you know the state of the DB when you make these changes.

There is a performance hit here, but if you need to be certain, it's worth it.

## Consistency (foreign keys)

```sql
create table parent (pid integer primary key);
insert into parent values (1), (2);
create table child (cid, integer primary key, pid integer);
INSERT INTO child SELECT i, 1 FROM generate_series(1, 1000000) AS i;
Alter table child add foreign key (pid) references parent;
delete from parent where pid = 1;
-- fails due to foreign key
delete from parent where pid = 2;
-- this takes too long ...? why
```

In explain analyze, first parens is the estimate, 2nd is the actual.

Foreign keys are implemented with triggers, which is what took so much of that time for deletion. When the delete happens, we much be sure no child exists with that foreign key. This includes running a select statement on child, to find any, since there is no index on the child this does a seq scan on 1 million rows. Lesson, ALWAYS create an index on the foreign key constraint;

```sql
create index on child (pid)
delete from parent where pid = 2;
-- now super fast
```

## Vaccuum

[Vacuum docs](https://www.postgresql.org/docs/9.1/sql-vacuum.html)

`VACUUM (VERBOSE) account;`

This cleans up the account table removing old "row versions" the dead row versions created that we saw when looking at ctid. It DOES NOT make the table smaller, it just frees the space in the table, this is now available for future updates/inserts. We discuss "table bloat", this is normal, room for the table to grow.

It would not be good if we had to do this ourselves.

if you checkout `ps -fu postgres` you see the `postgres: autovacuum launcher`, it detects a certain percentage of the table is dead rows, then it goes and vacuums in the background to free the space for us. Remember, the table does not get smaller, just emptier.

Normally you don't notice the vacuum or need to know about it.

If you do something like a batch write (1000000 rows), then those rows are eventually deleted, you can explicitely run the vacuum so you keep reusing the same row space without increasing bloat.

In order to get an index only scan, you must have had a recently vacuumed table.
Pay attention to `Heap Fetches` in the explain, it says how many times the scan actually went to the table to check things like fmin and fmax. The lower the heap fetch, the more recently the table was vacuumed. If we want a suuuuuper fast query, first vacuum the table or have it regularly vacuumed.

* Pre postgres 11 `create index on account (id, name);

* Postgres 11+ `create index on account (id) include (name); -- this is an index for an index only scan, we only ever scan id, and name is just the payload to be returned (so we don't have to go to the table to get it).

* We want autovacuum to run more often but only on a specific table; `ALTER TABLE account SET (autovacuum_vacuum_scale_factor = 0.01);` -- default is 0.2 (20% of the table is dead)

`\d pg_stat_all_tables` you can watch for the ratio of live to dead tuples, this might point to making autovacuum faster.

`TABLE pg_stat_activity;` shows all active sessions in the DB

### Heap Only Tuple updates (HOT updating)

```sql
CREATE TABLE hot (id bigint PRIMARY KEY, val text) with (fillfactor = 70) -- 30% of each block will be left empty

-- update only non-indexed columns
```

This allows for high update workloads in Postgres.

## Auditting

You could set a trigger to write to a history table for every write to a given table, this would insert into a new table.

# Functions

A function is essentially procedural code in the database.

(Stored Procedures)

`CREATE EXTENSION` tab shows extentions.

```sql
\h CREATE FUNCTION

CREATE FUNCTION doubleme(i integer) RETURNS integer LANGUAGE sql AS 'SELECT i * 2';

SELECT doubleme(21);
```