# Day 4 - Security & Partitioning

`\h CREATE ROLE` (CREATE USER is the same, but with default LOGIN, role defaults to NOLOGIN)

Don't add a user with a password like this:

```sql
CREATE ROLE joe LOGIN PASSWORD 'secret';
```

It will end up in the psql history, it will be sent to the server, it will be easy to maliciously access it with the right access.

Let's create one without a password.

```sql
postgres# CREATE ROLE steve LOGIN;
CREATE ROLE
postgres# \password steve
Enter new password:
Enter it again:

postgres# CREATE ROLE logistics;
CREATE ROLE
postgres# GRANT logistics TO joe;
ERROR:  role "joe" does not exist
postgres# \du
                                    List of roles
  Role name  │                         Attributes                         │ Member of
═════════════╪════════════════════════════════════════════════════════════╪═══════════
 logistics   │ Cannot login                                               │ {}
 steve       │                                                            │ {}
 steveabrams │ Superuser, Create role, Create DB, Replication, Bypass RLS │ {}
```

Every DB object has an owner, only an owner may alter and drop an object and grant priveleges to others. They can delegate granting (not recommended)


## Schemas

```sql
\dx -- list extentions

\dn -- list schemas (namespaces)

   List of schemas
  Name  │    Owner
════════╪═════════════
 public │ steveabrams
(1 row)

-- most of the time you don't pay attention to this concept but they can be interesting
```

Schemas are good for organizing things, only one level (not like a directory system). You can organize your tables/objects in schemas.

Schemas are also helpful because they have permissions associated with them (CREATE, USAGE)

`CREATE SCHEMA myapp;`

```sql
course# \dn+
                              List of schemas
  Name  │    Owner    │     Access privileges      │      Description
════════╪═════════════╪════════════════════════════╪════════════════════════
 myapp  │ steveabrams │                            │
 public │ steveabrams │ steveabrams=UC/steveabrams↵│ standard public schema
        │             │ =UC/steveabrams            │
(2 rows)
```


`steveabrams=` - Roll to which the privelege is granted
`UC -- Priveleges: Usage and Create`
`/steveabrams` - Roll that granted the privelege

The second privelege doesn't have a name before the `=`, it means it's granted to EVERYONE, very dangerous. Anyone can create tables in this schema.

First thing you should do after creating a new postgresql DB:

`REVOKE CREATE ON SCHEMA public FROM PUBLIC;`

Separate databases are strictly separated (can't access obj in db2 while logged into db1). Schemas are in the same DB, you can even do things like join two tables from separate schemas.

`myapp` has the "default" priveleges.

```sql
GRANT USAGE ON SCHEMA myapp TO logistics;` -- better to grant to a group rather than a role/user
```


A function that fails when given single quotes can be attacked with sql injection:

```sql
CREATE FUCNTION public count_rows(tablename text)
  returns bigint
  language plpgsql
as $function$
declare
  sql text;
  res bigint;
begin
  sql := 'select count(*) from ' || tablename;
  execute sql into res;
  return res;
end;
$function$


SELECT count_rows('''');

`Error: unterminated quoted string at or near "'"`
`Query: Select count(*) from '`


select count_rows('test; select count(*) from pg_user where usesuper');

count_rows
__________
         1
(1 row)

-- to prevent this do not construct sql yourself if you can avoid it.
```

```sql
\ef func_name -- edit function
```

## Partitioning

Normally a table is backed by a file (usually one file, could be more if it's big enough). A partition table is a fake table in a way (just a front, no data is actually stored in it, kind of like a view in that it looks/feels/behaves like a table, but it's an empty shell). The data is hidden behind the table in the "partitions". Partitions are just files too, there is different data in each partition. We need to define some rules on how to divide the data.

The partition table has a partitioning key, which determines which partition a row will go to.

There are many ways to implement. Some DBs hide the partitions. In Postgres, each partition is just a regular table.

Partitioning is fairly new feature (v10+).

Recommendation, if you want to use partitioning, use v12 at least, particularly performance is improved considerably.

There are 3 strategies for partitioning:

1. Range partitioning (most frequently used).

You have a column that is a timestamp, you partition by month or day perhaps

2. List partitioning

You ennumarate the values that go into each partition, no range specified. Good for partitioning by customer or similar.

3. Hash partitioning (least important)

You run a hash function, it spits out a number, you divide by a number (5 for example), take the remainder and each is a partition (so 5 total). Ends up with random spread, good for I/O distribution.

### Range partitioning

```sql
CREATE TABLE part (id bigint NOT NULL, createdat timestamp with time zone NOT NULL, data text) PARTITION BY RANGE (createdat);

INSERT INTO part VALUES (1, current_timestamp, 'something');
-- ERROR:  no partition of relation "part" found for row
-- DETAIL:  Partition key of the failing row contains (createdat) = (2020-04-30 11:10:25.184813-06).

CREATE TABLE part_2020 PARTITION OF part FOR VALUES FROM ('2020-01-01 00:00:00') TO ('2021-01-01 00:00:00');
CREATE TABLE part_2019 PARTITION OF part FOR VALUES FROM ('2019-01-01 00:00:00') TO ('2020-01-01 00:00:00');
CREATE TABLE part_2018 PARTITION OF part FOR VALUES FROM ('2018-01-01 00:00:00') TO ('2019-01-01 00:00:00');

INSERT INTO part VALUES (1, current_timestamp, 'something');

\d+ part

                                              Table "public.part"
  Column   │           Type           │ Collation │ Nullable │ Default │ Storage  │ Stats target │ Description
═══════════╪══════════════════════════╪═══════════╪══════════╪═════════╪══════════╪══════════════╪═════════════
 id        │ bigint                   │           │ not null │         │ plain    │              │
 createdat │ timestamp with time zone │           │ not null │         │ plain    │              │
 data      │ text                     │           │          │         │ extended │              │
Partition key: RANGE (createdat)
Partitions: part_2018 FOR VALUES FROM ('2018-01-01 00:00:00-07') TO ('2019-01-01 00:00:00-07'),
            part_2019 FOR VALUES FROM ('2019-01-01 00:00:00-07') TO ('2020-01-01 00:00:00-07'),
            part_2020 FOR VALUES FROM ('2020-01-01 00:00:00-07') TO ('2021-01-01 00:00:00-07')


EXPLAIN SELECT * FROM part_2020;
                          QUERY PLAN
══════════════════════════════════════════════════════════════
 Seq Scan on part_2020  (cost=0.00..20.70 rows=1070 width=48)
(1 row)

EXPLAIN SELECT * FROM part;

                             QUERY PLAN
════════════════════════════════════════════════════════════════════
 Append  (cost=0.00..78.15 rows=3210 width=48)
   ->  Seq Scan on part_2018  (cost=0.00..20.70 rows=1070 width=48)
   ->  Seq Scan on part_2019  (cost=0.00..20.70 rows=1070 width=48)
   ->  Seq Scan on part_2020  (cost=0.00..20.70 rows=1070 width=48)
(4 rows)

CREATE TABLE part_def PARTITION OF part DEFAULT; -- anything not for another partition table will end up here

-- this is not the best if you plan on adding other partitions, for example if you added a 2021 date to the above then add a 2021 partition, an error will be thrown and you won't be able to add the new partition.

CREATE TABLE part_2021 (LIKE part_2020 INCLUDING ALL);
-- clones the 2020 partition to 2021 partition

-- move the data to the partition
INSERT INTO part_2021 SELECT * FROM part_def;

DELETE FROM part_def;

--now attach the partition
ALTER TABLE part ATTACH PARTITION part_2021 FOR VALUES FROM ('2021-01-01 00:00:00') TO ('2022-01-01 00:00:00');

ALTER TABLE part DETACH PARTITION part_2018;

-- the upper limit is exclusive (up to 2022 00:00:00 but not including it)
```

```sql
INSERT INTO part select i, timestamptz '2019-12-30 00:00:00' + i * INTERVAL '1 day', 'whatever' FROM generate_series(1, 400) as i;

EXPLAIN table part;

CREATE INDEX ON part (createdat);
-- adds index to each partition

ANALYZE part;

explain SELECT * from part where createdat between '2019-01-01' and '2020-01-04';
                                                                             QUERY PLAN
════════════════════════════════════════════════════════════════════════════════════════════
 Append  (cost=0.00..8.29 rows=5 width=25)
   ->  Seq Scan on part_2019  (cost=0.00..1.01 rows=1 width=25)
         Filter: ((createdat >= '2019-01-01 00:00:00-07'::timestamp with time zone) AND (createdat <= '2020-01-04 00:00:00-07'::timestamp with time zone))
   ->  Bitmap Heap Scan on part_2020  (cost=4.19..7.25 rows=4 width=25)
         Recheck Cond: ((createdat >= '2019-01-01 00:00:00-07'::timestamp with time zone) AND (createdat <= '2020-01-04 00:00:00-07'::timestamp with time zone))
         ->  Bitmap Index Scan on part_2020_createdat_idx  (cost=0.00..4.19 rows=4 width=0)
               Index Cond: ((createdat >= '2019-01-01 00:00:00-07'::timestamp with time zone) AND (createdat <= '2020-01-04 00:00:00-07'::timestamp with time zone))
(7 rows)

-- It only used two indexes. Partition pruning. this improves performance, you can see it made a different plan for each partition.
```

```sql
ALTER TABLE part ADD PRIMARY KEY (id);
--ERROR:  insufficient columns in PRIMARY KEY constraint definition
--DETAIL:  PRIMARY KEY constraint on table "part" lacks column "createdat" which is part of the partition key.

-- the partitioning key needs to be part of the primary key.
ALTER TABLE part ADD PRIMARY KEY (id, createdat);

--v11 FK pointing from part table to other table
--v12 FK pointing to part table from other table
```

##### What are partitions good for?

1. Cheaply and easily get rid of a lot of old data

2. Reduce cardinality of certain queries

3. Vacuum process - you can vacuum each individually
