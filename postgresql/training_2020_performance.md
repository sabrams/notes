# Day 5 - Performance

## Copy

First a note about copy

For taking data out of the database or copying data into the database

`\h COPY`

This runs on on the database server, this requires you to have server superuser permission.

You can copy on the client, but you need to use STDIN STDOUT

```sql
--shows the data
COPY part_2020 TO STDOUT
```

```sql
-- copies the data to this file
\COPY part_2020 TO 'clientfile'

-- copies data to a csv file
\COPY part_2020 TO 'clientfile' (FORMAT 'csv')
```

There are a few formats: binary, csv, etc...

## Query Performance Tuning

```sql
SHOW log_min_duration_statement;

-- any statement that takes longer than this will get logged

SHOW shared_preload_libraries

ALTER SYSTEM SET shared_preload_libraries = 'pg_stat_statements';
-- need to restart system for this
```

### Identifying new index possibilities
```sql

\d pg_stat_user_tables
-- gives lots of interesting info such as number of seq_scans which can identify potential new indexes

SELECT relname,
  seq_scan,
  seq_tup_read,
  seq_tup_read::float8 / seq_scan AS tup_per_scan -- how many rows were returned on average for each seq scan
FROM pg_stat_user_tables
WHERE seq_scan > 0
ORDER BY tup_per_scan DESC
LIMIT 10;

  relname  │ seq_scan │ seq_tup_read │   tup_per_scan
═══════════╪══════════╪══════════════╪══════════════════
 test      │       46 │     50331648 │ 1094166.26086957
 part_2020 │        2 │          734 │              367
 part_def  │        2 │           66 │               33
 json      │        6 │           10 │ 1.66666666666667
 account   │        5 │            6 │              1.2
 part_2019 │        3 │            3 │                1
 person    │        2 │            0 │                0
 part_2018 │        2 │            0 │                0
(8 rows)

-- We are worried when we see a high seq_scan and a high_tup_read. a small seq scan with a high tup read isn't bad, and vice versa, a large seq scan with low tup read is ok too.

```

### Identifying too many indexes

```sql
\d pg_stat_user_indexes

-- idx_scan shows how many times an index was scanned

SELECT relname, indexrelname, pg_total_relation_size(indexrelid)
FROM pg_stat_user_indexes
WHERE idx_scan = 0
ORDER BY 3 DESC;
-- find size and name of all indexes that have never been scanned

-- we don't want to get rid of these always, sometimes they are ok even if they haven't scanned.

-- also could be good to find indexes that have low number of scans (WHERE idx_scan < 5)
```

### Improving queries

```sql
EXPLAIN SELECT * FROM pg_stats;

                                             QUERY PLAN
════════════════════════════════════════════════════════════════════════════════════════════════════
 Nested Loop Left Join  (cost=122.17..153.64 rows=6 width=401)
   ->  Hash Join  (cost=122.04..152.06 rows=6 width=475)
         Hash Cond: ((s.starelid = c.oid) AND (s.staattnum = a.attnum))
         ->  Seq Scan on pg_statistic s  (cost=0.00..24.98 rows=498 width=349)
         ->  Hash  (cost=108.28..108.28 rows=917 width=142)
               ->  Hash Join  (cost=21.52..108.28 rows=917 width=142)
                     Hash Cond: (a.attrelid = c.oid)
                     Join Filter: has_column_privilege(c.oid, a.attnum, 'select'::text)
                     ->  Seq Scan on pg_attribute a  (cost=0.00..79.51 rows=2751 width=70)
                           Filter: (NOT attisdropped)
                     ->  Hash  (cost=16.76..16.76 rows=381 width=72)
                           ->  Seq Scan on pg_class c  (cost=0.00..16.76 rows=381 width=72)
                                 Filter: ((NOT relrowsecurity) OR (NOT row_security_active(oid)))
   ->  Index Scan using pg_namespace_oid_index on pg_namespace n  (cost=0.13..0.18 rows=1 width=68)
         Index Cond: (oid = c.relnamespace)
(15 rows)

course# EXPLAIN (ANALYZE, BUFFERS) SELECT * FROM pg_stats;
                                                                   QUERY PLAN
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
 Nested Loop Left Join  (cost=122.17..153.64 rows=6 width=401) (actual time=6.946..8.988 rows=412 loops=1)
   Buffers: shared hit=1969 read=19 dirtied=9
   ->  Hash Join  (cost=122.04..152.06 rows=6 width=475) (actual time=6.922..8.288 rows=412 loops=1)
         Hash Cond: ((s.starelid = c.oid) AND (s.staattnum = a.attnum))
         Buffers: shared hit=1145 read=19 dirtied=9
         ->  Seq Scan on pg_statistic s  (cost=0.00..24.98 rows=498 width=349) (actual time=0.031..1.087 rows=412 loops=1)
               Buffers: shared hit=17 read=3 dirtied=6
         ->  Hash  (cost=108.28..108.28 rows=917 width=142) (actual time=6.864..6.864 rows=2776 loops=1)
               Buckets: 4096 (originally 1024)  Batches: 1 (originally 1)  Memory Usage: 504kB
               Buffers: shared hit=1125 read=16 dirtied=3
               ->  Hash Join  (cost=21.52..108.28 rows=917 width=142) (actual time=0.317..5.576 rows=2776 loops=1)
                     Hash Cond: (a.attrelid = c.oid)
                     Join Filter: has_column_privilege(c.oid, a.attnum, 'select'::text)
                     Buffers: shared hit=1125 read=16 dirtied=3
                     ->  Seq Scan on pg_attribute a  (cost=0.00..79.51 rows=2751 width=70) (actual time=0.019..2.379 rows=2776 loops=1)
                           Filter: (NOT attisdropped)
                           Buffers: shared hit=36 read=16 dirtied=3
                     ->  Hash  (cost=16.76..16.76 rows=381 width=72) (actual time=0.258..0.258 rows=381 loops=1)
                           Buckets: 1024  Batches: 1  Memory Usage: 47kB
                           Buffers: shared hit=12
                           ->  Seq Scan on pg_class c  (cost=0.00..16.76 rows=381 width=72) (actual time=0.005..0.177 rows=381 loops=1)
                                 Filter: ((NOT relrowsecurity) OR (NOT row_security_active(oid)))
                                 Buffers: shared hit=12
   ->  Index Scan using pg_namespace_oid_index on pg_namespace n  (cost=0.13..0.18 rows=1 width=68) (actual time=0.001..0.001 rows=1 loops=412)
         Index Cond: (oid = c.relnamespace)
         Buffers: shared hit=824
 Planning Time: 0.544 ms
 Execution Time: 9.107 ms
(28 rows)

-- rows "how many rows returned"
-- loops means "how often was this node executed
-- We see rows = 412 up top, for nexted rows, we need to loop that many times, which we see with loops=412
```

Buffers

We hit 1969 blocks, read from 19 of them, and dirtied 9 of them. These numbers are cumulative.

Look at which node had the most time spent in it (might not be the problem, but could be the symptom). Look at the estimated vs actual row count. If they are close together then we are ok, PG was able to make the best decision based on the estimate. If they are far apart, then it's a problem. Good to also look at the first node where the estimate is different than the actual.

### Joins

   &nbsp;         | Nested Loop                            | Hash Join                                                                      | Merge Join
------------------| ---------------------------------------| -------------------------------------------------------------------------------| -----------
How does it work? | Well, nested loops                     | Seq scan on inner table, build hash table, seq scan on outer table, probe hash | Sort both tables on join condition, scan and join the result (like card shuffling)
When is it good?  | Outer table is small                   | For bigger tables when hash <= `work_mem`                                      | Large tables
Do indexes help?  | Index on join condition on inner table | No index will speed this up                                                    | Index on join condition on both tables

Where does explain cost come from?

```sql
explain select * from test;
                          QUERY PLAN
═══════════════════════════════════════════════════════════════
 Seq Scan on test  (cost=0.00..64615.04 rows=4194304 width=14)
(1 row)

select name, setting, short_desc from pg_settings where name like '%_cost';

          name           │ setting │                                             short_desc
═════════════════════════╪═════════╪════════════════════════════════════════════════════════════════════════════════════════════════════
 cpu_index_tuple_cost    │ 0.005   │ Sets the planner's estimate of the cost of processing each index entry during an index scan.
 cpu_operator_cost       │ 0.0025  │ Sets the planner's estimate of the cost of processing each operator or function call.
 cpu_tuple_cost          │ 0.01    │ Sets the planner's estimate of the cost of processing each tuple (row).
 jit_above_cost          │ 100000  │ Perform JIT compilation if query is more expensive.
 jit_inline_above_cost   │ 500000  │ Perform JIT inlining if query is more expensive.
 jit_optimize_above_cost │ 500000  │ Optimize JITed functions if query is more expensive.
 parallel_setup_cost     │ 1000    │ Sets the planner's estimate of the cost of starting up worker processes for parallel query.
 parallel_tuple_cost     │ 0.1     │ Sets the planner's estimate of the cost of passing each tuple (row) from worker to master backend.
 random_page_cost        │ 4       │ Sets the planner's estimate of the cost of a nonsequentially fetched disk page.
 seq_page_cost           │ 1       │ Sets the planner's estimate of the cost of a sequentially fetched disk page.
(10 rows)


select reltuples, relpages from pg_class where relname = 'test';

 reltuples  │ relpages
════════════╪══════════
 4.1943e+06 │    22672
(1 row)

select 4.1943e+06 * 0.01 + 22781 * 1;

 ?column?
══════════
 64724.00 -- 64615.04 was the cost in the explain.
(1 row)
```
