# Day 1 - Indexes

Explain width is the width of the results in bytes

Explain const is a relative number that postgres uses to describe effort

Indexes increase query performance while decreasing create/update performance.

`psql postres -U steveabrams`

```sql
set enable_seqscan = off;  -- force index usage
reset enable_seqscan;

ANALZYE table; -- updates explain estimates

\? -- command options
\h -- help options
\l -- list databases
\c databasename -- use specific database
\d tablename -- table describe
\ds -- sequence describe
\di -- describe indexes
\di+ index_name -- describe index in detail
\x expanded display

TABLE tablename; -- equal to select * from tablename;

SELECT * FROM pg_stats where tablename = 'table-name' AND attname = 'col-name';
-- shows meta row info as ratios of null vs other etc
```

Btree is best when you can compare and choose one value over another

Using ranges for times could be super cool, there is an overlap feature you can index on.

## Trigram indexes

Allows for "similar to" indexing

`create extension pg_trgm;`

`select * from test where name % 'stepen'` will match `steven`

Building gin indexes take a while and are slower to update

`CREATE INDEX ON test USING gin (name gin_trgm_ops);`

Great for things like `SELECT * FROM test where name LIKE '%eve';`

Also great from regex searching `SELECT * FROM test WHERE name ~ 'ste(v|ph)en';`

To inspect how it is indexed, `select show_trgm('steven');`

- Great for fuzzy search

### Limitations

- They become large fast
- Short strings doesn't make sense
- Slow to create and update

Normally GIN indexes have `fastupdate`, you can set it to off, it will make searches even faster, but will cause updates inserts to be slower.

- With i18n, some asian langauges suffer here because they may have a single character that means many things, where in english or similar languages, there are more characters so the index makes sense.

## To create a table with millions of rows

```sql
CREATE TABLE test (id bigint GENERATED ALWAYS AS IDENTITY NOT NULL, name text);
INSERT INTO test (name) VALUES ('hans');
INSERT INTO test (name) VALUES ('paul');
INSERT INTO test (name) SELECT name FROM test;
INSERT INTO test (name) SELECT name FROM test;
INSERT INTO test (name) SELECT name FROM test;
-- repeat until you hit a few million
```