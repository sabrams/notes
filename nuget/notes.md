## Nuget feed initial look

* A feed is not what we think it is. It is a directory on a specifically configured server that you can tell nuget to connect to and use as a package registry. There are specific servers this is written for (s3, azure, nuget-server). If we want to do the "quick and easy" feed, we would set up a nuget-server and allow users to connect to it as their "feed", think of this solution like how we use docker registry as our container registry. We would be using "nuget registry". We would not have control over much of the storage and access to info. I do not know if it's a good idea.

https://docs.microsoft.com/en-us/nuget/hosting-packages/nuget-server

* There is dotnet and nuget.exe, and others, but they all use the same registry API, so we just have to implement those API endpoints, and that should allow us to work with all of those platforms. It looks like there are 9 endpoints to implement, but it's possible we might only have to implement half for mvc, although we can't know this until we test it and see which endpoints it pings. Either way, much less than the 16 from Conan. The docs are pretty nice, they actually say:

1) what the endpoint is
2) what a request looks like
3) what a response looks like

I think the biggest challenge here will be testing (no one on our team has ever used nuget packages or written c#). But because the docs are thorough, I think we could go a long ways without even worrying about testing.

- Naming conventions, I think we all agree on self-managed, but what do we do for .com