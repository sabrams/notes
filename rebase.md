# Rebase trick

`git rebase --onto newbase oldbase`

- New base is the master commit you want to put your code on
- Old base is the commit you no longer want at the base of your feature branch (you replace this with the new base)