# Mitmproxy

[Docs](https://docs.mitmproxy.org/stable/)

Start the proxy web interface:

```
mitmweb
```

Bundle install via the proxy:
```
SSL_CERT_FILE=~/.mitmproxy/mitmproxy-ca-cert.pem http_proxy=http://127.0.0.1:8080 bundle install --verbose
```

Troubleshooting:

Make sure nginx or other services aren't running anything on port 8080.