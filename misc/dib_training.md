Intent vs Impact - Ask about impacet, don't just explain intent and _stay engaged_.

Say your lens outloud so others know. For example: "As a likely priveleged middle class male...".

It's not enough to just be a good person, you still will make these mistakes, you have to work on it.

1. Be consious & intentional
2. Be courageous - you will make mistakes
3. Have compassion - for yourself and others

Controversial topics are unavoidable if you want to be inclusive

Empathy does not mean agreement

Communication can break down if you want to get to solutions too soon - Empathy, connection, and common ground must come first.

Everyone normalizes their own experiences.

You don't need permission from a victim to be an upstander

Developing cross cultural intelligence:

   - Avoid sport metaphors, simplify technical and formal language